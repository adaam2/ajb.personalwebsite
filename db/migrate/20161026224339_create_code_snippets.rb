class CreateCodeSnippets < ActiveRecord::Migration[5.0]
  def change
    create_table :code_snippets do |t|
      t.string :title
      t.string :download_link
      t.string :repository_url
      t.text :description
      t.boolean :published
      t.text :setup_instructions
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
