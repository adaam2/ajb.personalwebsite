class AddWordpressFlagToArticle < ActiveRecord::Migration[5.0]
  def change
  	add_column :articles, :imported_from_wordpress, :boolean
  end
end
