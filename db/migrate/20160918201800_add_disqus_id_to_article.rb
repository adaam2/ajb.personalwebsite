class AddDisqusIdToArticle < ActiveRecord::Migration[5.0]
  def change
  	add_column :articles, :disqus_id, :string
  end
end
