case Rails.env
when 'development'
    # seed one admin user
    Article.delete_all
    AdminUser.delete_all
    AdminUser.create!(email: 'adamjamesbull@googlemail.com', password: 'butter', password_confirmation: 'butter')
    for i in 0..9
    	article = Article.create({title: 'test post', content: 'test post', published: true, created_at: rand(Date.civil(1990, 1, 1)..Date.civil(2016, 12, 31))})
    end
when 'production'
	# reindex all articles in production environment
	Article.reindex!
end


