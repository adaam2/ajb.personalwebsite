class Book
	attr_accessor :name, :author, :kind, :price

	def initialize(name, author, kind)
		@name = name
		@author = author
		@kind = kind
	end

	def to_s
		"'#{@name}' by #{@title} is a #{@kind} book"
	end

	def price_in_pennies
		Integer(price * 100)
	end
end
require 'csv'
class CsvReader
	def initialize
		@books_in_stock = []
	end

	def read_in_csv(csv_file_name)
		CSV.foreach(csv_file_name, encoding: 'bom|utf-8', col_sep: ',', row_sep: "\r\n") do |row|
			@books_in_stock << Book.new(row[0], row[1], row[2])
		end
	end
end

number_of_args = ARGV.size

# puts "there were #{number_of_args} args provided"

# name = ARGV[0]
# kind = ARGV[1]

# b = Book.new
# b.name = 'to killing a mocking bird'
# b.author = 'harper lee'
# b.kind = 'fiction'
# b.price = 12.99

# puts b.price_in_pennies

reader = CsvReader.new

file_name = ARGV[0]

reader.read_in_csv file_name

puts reader.books_in_stock.size


