require File.expand_path('config/environments/development.rb')

puts 'Setting all articles to unpublished'

Article.update_all(published: false)

puts 'Updated all'