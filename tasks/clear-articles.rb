require File.expand_path('config/environments/development.rb')

puts 'Begin removal of all articles...'

deleted_count = Article.delete_all

puts "There were #{deleted_count} articles to remove"