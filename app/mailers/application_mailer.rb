class ApplicationMailer < ActionMailer::Base
  default from: 'hello@adamjamesbull.co.uk'
  layout 'mailer'
end
