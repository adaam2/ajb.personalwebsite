class Article < ApplicationRecord
	# Relationships

	# Validations
	validates :title, presence: true, length: { minimum: 5 }

	# Callbacks
  	before_save :generate_slug
  	after_destroy :cleanup_after_deletion

	# Scopes
	default_scope { order( created_at: :desc ) }

	# Third party
	extend FriendlyId
	friendly_id :title, use: :slugged
	include AlgoliaSearch

	algoliasearch do
	    attributes :title, :content
	    attributesToIndex %w(title content)
	end
	
	acts_as_taggable_on :tags
	acts_as_paranoid
	is_impressionable
	
	# Methods

	private
		def cleanup_after_deletion
			puts "Deleted article"
		end

	  	def generate_slug
		    if self.slug.present? && self.slug == title.parameterize
		      self.slug
		    else
		      self.slug = title.parameterize
			end
		end

	  	# Accessor method to determine if a new friendly id should be generated
	  	def should_generate_new_friendly_id?
	    	new_record?
	  	end

		# the field candidates for the slug
		def slug_candidates
	    [
	      :title
	    ]
  end
end
