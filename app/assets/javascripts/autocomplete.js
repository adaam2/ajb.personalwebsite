$(document).ready(function() {
    $('.tagselect').each(function() {
        var placeholder = $(this).data('placeholder');
        var url = $(this).data('url');
        var saved = $(this).data('saved');
        var select = $(this).select2({
            tags: true,
            placeholder: placeholder,
            minimumInputLength: 1,
            initSelection : function(element, callback){
                saved && callback(saved);
            },
            multiple: true,
            allowClear: true,
            ajax: {
                url: url,
                dataType: 'json',
                data:    function(term) { return { q: term }; },
                results: function(data) { return { results: data }; }
            },
            createSearchChoice: function(term, data) {
                if ($(data).filter(function() {
                    return this.name.localeCompare(term)===0;
                }).length===0) {
                    return { id: term, name: term };
                }
            },
            tokenSeparators: [','],
            formatResult:    function(item, page){ return item.name; },
            formatSelection: function(item, page){ return item.name; }
        });


        $(this).on('change', function(e){
            var evt = e;

            var arr = $(this).val().split(',');

            var index = arr.indexOf(e.removed.name);

            if(index !== -1){
                arr.splice(index, 1);
            }
            $(this).val(arr.join(','));

            $(this).data(JSON.stringify(arr));

        });
    });
});