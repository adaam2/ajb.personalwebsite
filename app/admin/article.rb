ActiveAdmin.register Article, as: 'Word' do
permit_params :title, :content, :published, :tag_list

config.sort_order = 'created_at_desc'

filter :title
filter :created_at
filter :published
filter :imported_from_wordpress

index do
	column(:title)
  column(:published)

	actions defaults:true
end

form do |f|
	inputs 'Post details' do
    input :title
    input :content , :as => 'ckeditor', :ckeditor => { :uiColor => '#FFFFFF', :toolbar => 'mini' }
    input :published, as: :boolean
    f.input :tag_list,
      label:      'Tags',
      input_html: {
        data: {
          placeholder: 'Enter tags',
          saved:       f.object.tags.map{|t| {id: t.name, name: t.name}}.to_json,
          url:         autocomplete_tags_path },
        class: 'tagselect',
        value: f.object.tags.collect(&:name).map{|t| %Q(#{t})}.join(', ')
     }

  end
  actions
end

controller do
    def show
      redirect_to article_path(params[:id])
    end
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
    def autocomplete_tags
      @tags = ActsAsTaggableOn::Tag.
        where('name LIKE ?', "#{params[:q]}%").
        order(:name)
      respond_to do |format|
        format.json { render :json => @tags.collect{|t| {:id => t.name, :name => t.name }}}
      end
    end
  end
end