class TagsController < ApplicationController
  def index
    respond_to do |format|
      @tags = ActsAsTaggableOn::Tag.where('taggings_count > 0').order('taggings_count desc')
      format.html
      format.json { render json: @tags }
    end
  end

  def show
    # The ! will raise a RecordNotFound exception resulting in a 404 page if no record found for the given tag.
    @tag =  ActsAsTaggableOn::Tag.friendly.find_by!(slug: params[:slug])
    @query = params[:slug]
    unless @tag.nil?
        @articles = Article.tagged_with(@tag.name)
    end
  end
end
