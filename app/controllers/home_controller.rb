class HomeController < ApplicationController
	def index
		@recent_articles = Article.where('published = true').order('created_at DESC').limit(5)
	end
end
