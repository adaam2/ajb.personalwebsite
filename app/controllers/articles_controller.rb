class ArticlesController < ApplicationController
	def index
		by_month = Article.includes(:impressions).where('published = true').group_by { |m| m.created_at.beginning_of_month }
		per_page = 4
		@articles_by_month = Kaminari.paginate_array(by_month.sort.reverse).page(params[:page]).per(per_page)
		@page = params[:page] ? params[:page] : 1
		@total_pages = (by_month.length.to_f / per_page.to_f).ceil
	end

	def show
		@article = Article.includes(:impressions).friendly.find(params[:id])
		impressionist(@article)
		@impression_count = @article.impressionist_count(:filter=>:all)
	end

	private
		def article_params
			params.require(:article).permit(:title, :content, :slug, :tag_list)
		end
end
