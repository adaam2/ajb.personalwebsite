class SearchController < ApplicationController
  impressionist :actions=>[:index]
  def index
  	@q = params['q']
    return if @q.blank?
    @hits = Article.algolia_search(@q)
  end
end
