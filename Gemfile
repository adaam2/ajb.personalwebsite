source 'https://rubygems.org'
ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
# use postgresql as database
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
gem 'friendly_id', '~> 5.1.0' # Note: You MUST use 5.0.0 or greater for Rails 4.0+
# Use jquery as the JavaScript library
gem 'jquery-rails'
# rails console awesome prettifier
gem 'awesome_print', require:'ap'
# select2
gem 'select2-rails', '~> 3.5.9'
gem 'activeadmin_addons'
gem 'flattened_active_admin'
# pagination
gem 'will_paginate', '~> 3.1.0'
gem 'bourbon'
gem 'neat'
gem 'activeadmin_pagedown'

gem 'impressionist', github: 'charlotte-ruby/impressionist'
# exception logging
gem 'rollbar'
# generates indefinite articles for words
gem 'indefinite_article'

# soft delete
gem 'paranoia', '~> 2.2.0.pre'

# rich text editor + file upload
gem 'ckeditor', github: 'galetahub/ckeditor'
gem 'carrierwave'
# Fog provides Amazon S3 uploads from carrierwave
gem 'fog'

# Graphics processing
gem 'graphicsmagick'

# clipboard access for rails runner
gem 'clipboard'
gem 'ffi', :platforms => [:mswin, :mingw]

# image upload optimization
gem 'mini_magick'

# search functionality
gem 'algoliasearch-rails'

# tagging
gem 'acts-as-taggable-on'

# easy server error partials
gem 'gaffe'

# google analytics
gem 'google-analytics-rails', '1.1.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# active admin + admin authentication
gem 'devise'
gem 'activeadmin', '~> 1.0.0.pre4'
gem 'inherited_resources', github: 'activeadmin/inherited_resources'

# wordpress integration
gem 'rubypress'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :test do
  gem 'rspec-rails', '~> 3.5'
  gem 'rails-controller-testing'
end

group :development do
  # Access an IRB console on exception partials or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'faker'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'guard'
  gem 'guard-livereload', '~> 2.5', require: false
  gem 'better_errors'
  gem 'rb-readline'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
