Rails.application.routes.draw do
  # default / home
  root 'home#index'

  # ckeditor engine
  mount Ckeditor::Engine => '/ckeditor'
  
  # normal routes
  get 'cv/index'
  get 'about' => 'about#index'
  get 'cv' => 'cv#index'
  get 'home/index'
  get 'search' => 'search#index'
  get 'tags/index'


  # tag autocomplete routes
  get '/admin/autocomplete_tags',
  to: 'admin/words#autocomplete_tags',
  as: 'autocomplete_tags'

  
  # active admin routes
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  # resources
  resources :articles, :path => 'words'
  resources :tags, only: [:index, :show], param: :slug
end
